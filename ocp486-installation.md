# Openshift Enterprise 4.8.6 Installation

This installation targets a baremetal deployment with a control plane with 3x masters nodes, and a compute plane with 3x workers nodes. This deployment uses PXE for installation and booting.

- Prerequisites

  - [ ] [DNS Server](scripts/1-dns-server.sh)
  - [ ] [HAProxy Server](scripts/2-haproxy-server.sh)
  - [ ] [DHCP Server](scripts/3-dhcp-server.sh)
  - [ ] [Web Server](scripts/4-http-server.sh)
  - [ ] [PXE Server](scripts/5-pxe-server.sh)
  - [ ] [Gathering Installation Artifacts](scripts/6-gathering-artifacts.sh)

---
