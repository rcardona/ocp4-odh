## create htpasswd file with user name:password
> htpasswd -c -B -b users.htpasswd developer developer
>
> htpasswd -B -b users.htpasswd dev1 dev1
>
> htpasswd -B -b users.htpasswd dev2 dev2
>
> htpasswd -B -b users.htpasswd dev3 dev3
>
> htpasswd -B -b users.htpasswd admin1 admin1
>
> htpasswd -B -b users.htpasswd admin2 admin2
>

> htpasswd -B -b users.htpasswd dev1 dev1
>
> htpasswd -B -b users.htpasswd dev2 dev2
>
> htpasswd -B -b users.htpasswd dev3 dev3

oc get

> oc adm groups new universty-milan

> oc adm groups add-users universty-milan dev1 dev2 dev3


- ResourceQuota

      echo -e 'apiVersion: v1
      kind: ResourceQuota
      metadata:
        name: dev-quota
      spec:
        hard:
          memory: "4Gi"' | tee dev-quota.yml

- ClusterResourceQuota

      echo -e 'apiVersion: v1
      kind: ClusterResourceQuota
      metadata:
        name: dev-cluster-quota
      spec:
        quota:
          hard:
            memory: "4Gi"
        selector:
          labels:
            matchLabels:
              environment: "ml-projects"' | tee dev-cluster-quota.yml



> oc create --save-config -f dev-cluster-quota.yml


> oc new-project ml1

> oc label ns ml1 environment="ml-projects"

> oc adm policy remove-cluster-role-from-group self-provisioner system:authenticated:oauth


## assign the self-provisioner cluster role to the managers group.
  > oc adm policy add-cluster-role-to-group self-provisioner managers


## create new role

      apiVersion: v1
      kind: Role
      metadata:
        name: exampleview
      rules:
      - apiGroups: null
        resources:
        - pods
        - builds
        verbs:
        - get
          - list
          - watch


dev1

dev2

dev3

admin1

admin2
