#!/usr/bin/env bash

##############################################################
# UPDATE TO MATCH THE ENVIRONMENT
##############################################################

# OCP VERSION
OCPVERSION="4.8.6"
OCPREPORHCOS="https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.8/latest"

# OCP CLUSTER NAME AND DOMAIN ###
CLUSTER_NAME=ocp4
DOMAINNAME=telnet.nl
BASTION="bastion"

## THESE VARIABLES SHOULD BE UPDATED TO CONFIGURE DNS AND PROXY SERVERS ##
BASTION_IP="10.10.10.6"
BASTION_IP_FIRST_THREE_OCTET_INVERTED="10.10.10"
BASTION_IP_LAST_OCTET="6"
DNS_IP="10.10.10.253"
DNS_IP_LAST_OCTET="253"
API_IP="10.10.10.251"
API_IP_LAST_OCTET="251"
APPS_IP="10.10.10.252"
VPC_NETMASK="24"
BOOTSTRAP_IP="10.10.10.9"
BOOTSTRAP_IP_LAST_OCTET="9"
MASTER_0_IP="10.10.10.10"
MASTER_0_IP_FIRST_THREE_OCTET_INVERTED="10.10.10"
MASTER_0_IP_LAST_OCTET="10"
MASTER_1_IP="10.10.10.11"
MASTER_1_IP_FIRST_THREE_OCTET_INVERTED="10.10.10"
MASTER_1_IP_LAST_OCTET="11"
MASTER_2_IP="10.10.10.12"
MASTER_2_IP_FIRST_THREE_OCTET_INVERTED="10.10.10"
MASTER_2_IP_LAST_OCTET="12"
WORKER_0_IP="10.10.10.20"
WORKER_0_IP_LAST_OCTET="20"
WORKER_1_IP="10.10.10.21"
WORKER_1_IP_LAST_OCTET="21"
WORKER_2_IP="10.10.10.22"
WORKER_2_IP_LAST_OCTET="22"
