#!/bin/bash

source ocp4_install_env.sh.alt

# Configuration PXE server
sudo cp -r /usr/share/syslinux/* /var/lib/tftpboot
#for legacy BIOS systems
sudo mkdir -p /var/lib/tftpboot/pxelinux.cfg
sudo touch /var/lib/tftpboot/pxelinux.cfg/default
sudo chmod -R 0755 /var/lib/tftpboot/pxelinux.cfg
#for EFI only systems
sudo mkdir -p /var/lib/tftpboot/uefi
sudo cp -p /boot/efi/EFI/redhat/{shimx64.efi,grubx64.efi} /var/lib/tftpboot/uefi/
sudo touch /var/lib/tftpboot/uefi/grub.cfg
#kernel and initramfs are from artifacts
sudo chmod -R 0755 /var/lib/tftpboot/uefi
# sudo semanage port -l |grep -i http
sudo chcon -t tftpdir_rw_t /var/lib/tftpboot/*
sudo restorecon -FRv /var/lib/tftpboot


# now edit PXE configuration file with RHEL installation option. Note that all paths used in this file must be placed in /var/lib/tftpboot directory
sudo cat > /var/lib/tftpboot/pxelinux.cfg/default << EOF
DEFAULT menu.c32
TIMEOUT 300
PROMPT 0

MENU OCP PXE INSTALALTION Menu

LABEL bootstrap
  KERNEL http://${BASTION_IP}/rhcos-live-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION_IP}:8080/rhcos-live-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.live.rootfs_url=http://${BASTION_IP}:8080/rhcos-live-rootfs.x86_64.img coreos.inst.ignition_url=http://${BASTION_IP}:8080/bootstrap.ign

LABEL master
  KERNEL http://${BASTION_IP}/rhcos-live-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION_IP}:8080/rhcos-live-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.live.rootfs_url=http://${BASTION_IP}:8080/rhcos-live-rootfs.x86_64.img coreos.inst.ignition_url=http://${BASTION_IP}:8080/master.ign

LABEL worker
  KERNEL http://${BASTION_IP}/rhcos-live-kernel-x86_64
  APPEND ip=dhcp rd.neednet=1 initrd=http://${BASTION_IP}:8080/rhcos-live-initramfs.x86_64.img console=tty0 console=ttyS0 coreos.inst=yes coreos.inst.install_dev=sda coreos.live.rootfs_url=http://${BASTION_IP}:8080/rhcos-live-rootfs.x86_64.img coreos.inst.ignition_url=http://${BASTION_IP}:8080/worker.ign
EOF

sudo cat > /var/lib/tftpboot/uefi/grub.cfg <<UEFI
# This file is copied from
# https://github.com/coreos/fedora-coreos-config

set default="0"

function load_video {
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod all_video
}

load_video
set gfxpayload=keep
insmod gzio
insmod part_gpt
insmod ext2

set timeout=1000000
### END /etc/grub.d/00_header ###

### BEGIN /etc/grub.d/10_linux ###

menuentry 'Exit to local boot device' {
        exit
}

menuentry 'Install Red Hat Enterprise Linux CoreOS - BOOTSTRAP ' --class fedora --class gnu-linux --class gnu --class os {
	linuxefi uefi/rhcos-live-kernel-x86_64 console=tty0 console=ttyS0,115200n8 coreos.inst.install_dev=/dev/sda coreos.live.rootfs_url=http://${BASTION_IP}:8080/rhcos-live-rootfs.x86_64.img coreos.inst.ignition_url=http://${BASTION_IP}:8080/bootstrap.ign rd.neednet=1 bond=bond0:eno1,eno2:mode=802.3ad:xmit_hash_policy=layer3+4:lacp_rate=1:xmit_hash_policy=layer3+4 ip=bond0:dhcp
	initrdefi uefi/rhcos-live-initramfs.x86_64.img
}

menuentry 'Install Red Hat Enterprise Linux CoreOS - MASTER ' --class fedora --class gnu-linux --class gnu --class os {
        linuxefi uefi/rhcos-live-kernel-x86_64 console=tty0 console=ttyS0,115200n8 coreos.inst.install_dev=/dev/sda coreos.live.rootfs_url=http://${BASTION_IP}:8080/rhcos-live-rootfs.x86_64.img coreos.inst.ignition_url=http://${BASTION_IP}:8080/master.ign rd.neednet=1 bond=bond0:eno1,eno2:mode=802.3ad:xmit_hash_policy=layer3+4:lacp_rate=1:xmit_hash_policy=layer3+4 ip=bond0:dhcp
        initrdefi uefi/rhcos-live-initramfs.x86_64.img
}

menuentry 'Install Red Hat Enterprise Linux CoreOS - WORKER ' --class fedora --class gnu-linux --class gnu --class os {
        linuxefi uefi/rhcos-live-kernel-x86_64 console=tty0 console=ttyS0,115200n8 coreos.inst.install_dev=/dev/sda coreos.live.rootfs_url=http://${BASTION_IP}:8080/rhcos-live-rootfs.x86_64.img coreos.inst.ignition_url=http://${BASTION_IP}:8080/worker.ign rd.neednet=1 bond=bond0:eno1,eno2:mode=802.3ad:xmit_hash_policy=layer3+4:lacp_rate=1:xmit_hash_policy=layer3+4 ip=bond0:dhcp
        initrdefi uefi/rhcos-live-initramfs.x86_64.img
}


UEFI

# Allow Necessary Services and Ports in Firewalld
sudo firewall-cmd --permanent --add-service={tftp,ftp}
sudo firewall-cmd --permanent --add-port={69/udp,4011/udp}
sudo firewall-cmd --reload -q
sudo systemctl enable --now tftp.service -q
sudo systemctl restart tftp.service -q
