#!/bin/bash

# configuring dns server

source ocp4_install_env.sh

sudo chgrp named -R /var/named
sudo chown -v root:named /etc/named.conf
sudo restorecon -rv /var/named
sudo restorecon /etc/named.conf
sudo firewall-cmd --permanent --add-port=53/tcp
sudo firewall-cmd --permanent --add-port=53/udp
sudo firewall-cmd --permanent --add-port=53/tcp -q
sudo firewall-cmd --reload -q

sudo cat > /etc/named.conf << EOF
options {
        listen-on port 53 { 127.0.0.1; ${BASTION_IP}; };
        directory       "/var/named";
        allow-query     { localhost; any; };
        forwarders {
              130.186.1.53;
              130.186.84.244;
              8.8.8.8;
              8.8.4.4;
      };
};
zone "${DOMAINNAME}" IN {
      type master;
      file "/var/named/forward-${DOMAINNAME}.db";
      allow-update { none; };
};
zone "${MASTER_0_IP_FIRST_THREE_OCTET_INVERTED}.in-addr.arpa" IN {
      type master;
      file "/var/named/reverse-${DOMAINNAME}.db";
      allow-update { none; };
};
EOF

sudo cat > /var/named/forward-${DOMAINNAME}.db << EOFA
\$TTL 1W
@   IN  SOA     dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                            14000    ;Serial
                                            3H      ;Refresh
                                            15M     ;Retry
                                            1W      ;Expire
                                            1D      ;Minimum TTL
                                            )
; Name Server Information
@ IN NS dns.${DOMAINNAME}.
;
; IP address of Name Server
dns IN A ${BASTION_IP}
;
; Cluster APIs and Wildcard Records (all of them point to the loadbalancer)
api.${CLUSTER_NAME} IN A ${API_IP}
api-int.${CLUSTER_NAME} IN A ${API_IP}
*.apps.${CLUSTER_NAME} IN A ${APPS_IP}
;
; Create entry for the bootstrap host
bootstrap.${CLUSTER_NAME} IN A ${BOOTSTRAP_IP}
;
; Bastion and Registry
registry IN A ${BASTION_IP}
bastion IN A ${BASTION_IP}
;
; Control Panel Records
master-0.${CLUSTER_NAME} IN A ${MASTER_0_IP}
master-1.${CLUSTER_NAME} IN A ${MASTER_1_IP}
master-2.${CLUSTER_NAME} IN A ${MASTER_2_IP}
;
; Compute Records
worker-0.${CLUSTER_NAME} IN A ${WORKER_0_IP}
worker-1.${CLUSTER_NAME} IN A ${WORKER_1_IP}
worker-2.${CLUSTER_NAME} IN A ${WORKER_2_IP}
;
EOFA

sudo cat > /var/named/reverse-${DOMAINNAME}.db << EOFB
\$TTL 1W
@ IN SOA dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                             14000    ;Serial
                                             3H      ;Refresh
                                             15M     ;Retry
                                             1W      ;Expire
                                             1D      ;Minimum TTL
                                             )
; Name Server Information
@ IN NS dns.${DOMAINNAME}.
;
; Reverse lookup for cluster nodes
;
${BASTION_IP_LAST_OCTET} IN PTR dns.${DOMAINNAME}.
;
${BOOTSTRAP_IP_LAST_OCTET} IN PTR bootstrap.${CLUSTER_NAME}.${DOMAINNAME}.
;
${API_IP_LAST_OCTET} IN PTR api.${CLUSTER_NAME}.${DOMAINNAME}.
${API_IP_LAST_OCTET} IN PTR api-int.${CLUSTER_NAME}.${DOMAINNAME}.
;
${MASTER_0_IP_LAST_OCTET} IN PTR master-0.${CLUSTER_NAME}.${DOMAINNAME}.
${MASTER_1_IP_LAST_OCTET} IN PTR master-1.${CLUSTER_NAME}.${DOMAINNAME}.
${MASTER_2_IP_LAST_OCTET} IN PTR master-2.${CLUSTER_NAME}.${DOMAINNAME}.
;
${WORKER_0_IP_LAST_OCTET} IN PTR worker-0.${CLUSTER_NAME}.${DOMAINNAME}.
${WORKER_1_IP_LAST_OCTET} IN PTR worker-1.${CLUSTER_NAME}.${DOMAINNAME}.
${WORKER_2_IP_LAST_OCTET} IN PTR worker-2.${CLUSTER_NAME}.${DOMAINNAME}.
EOFB


sudo systemctl enable --now named -q
sudo systemctl restart named -q
