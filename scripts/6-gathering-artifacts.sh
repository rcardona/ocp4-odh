#!/bin/bash

source ocp4_install_env.sh

# download artifacts
sudo mkdir -p /artifacts
cd /artifacts
sudo wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.8.22/openshift-install-linux.tar.gz
sudo mkdir -p /usr/local/bin
sudo tar xzfv openshift-install-linux.tar.gz -C /usr/local/bin
sudo wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.8.22/openshift-client-linux.tar.gz
sudo tar xzfv openshift-client-linux.tar.gz -C /usr/local/bin

wget https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.8/latest/rhcos-live-initramfs.x86_64.img
wget https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.8/latest/rhcos-live-kernel-x86_64
wget https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.8/latest/rhcos-live-rootfs.x86_64.img

# complete the configuration for EFI grub PXE
sudo cp -p {rhcos-live-kernel-x86_64,rhcos-live-initramfs.x86_64.img} /var/lib/tftpboot/uefi/
sudo chmod -R 0644 /var/lib/tftpboot/uefi/{rhcos-live-kernel-x86_64,rhcos-live-initramfs.x86_64.img}
