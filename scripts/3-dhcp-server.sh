#!/bin/bash

source ocp4_install_env.sh

sudo cat > /etc/dhcp/dhcpd.conf << EOF
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

subnet 10.10.10.0 netmask 255.255.255.0 {
	option routers 10.10.10.254;
	range 10.10.10.2 10.10.10.253;

  class "pxeclients" {
      match if substring (option vendor-class-identifier, 0, 9) = "PXEClient";
      next-server ${BASTION_IP};

      if option architecture-type = 00:07 {
        filename "uefi/shimx64.efi";
      } else {
        filename "pxelinux.0";
      }
  }

  host master-0 {
      hardware ethernet 0C:42:A1:9F:4D:10;
      fixed-address 10.10.10.10;
  }

  host master-1 {
      hardware ethernet 0C:42:A1:9F:38:A8;
      fixed-address 10.10.10.11;
  }

  host master-2 {
      hardware ethernet 0C:42:A1:DA:BA:AE;
      fixed-address 10.10.10.12;
  }

  host worker-0 {
      hardware ethernet 0C:42:A1:FA:23:9A;
      fixed-address 10.10.10.20;
  }

  host worker-1 {
      hardware ethernet 0C:42:A1:FA:23:FA;
      fixed-address 10.10.10.21;
  }

  host worker-2 {
      hardware ethernet 0C:42:A1:FA:2B:4A;
      fixed-address 10.10.10.22;
  }

  host bootstrap {
      hardware ethernet 0C:42:A1:FA:2B:4A;
      fixed-address 10.10.10.22;
  }

}
EOF


sudo firewall-cmd --permanent --add-service=dhcp
sudo firewall-cmd --reload
sudo systemctl enable --now dhcpd -q
sudo systemctl restart dhcpd -q
