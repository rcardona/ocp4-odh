# Open Data Hub 1.1 on Openshift Enterprise 4.8.6


## Getting started

This repo gathers the steps carried to deploy Open Data Hub and Anaconda Data Science Toolkit on Openshift Enterprise.

## Environment

- Platform: Baremetal idRAC

- Software Stack:

    - [Openshift Enterprise 4.8.6](https://docs.openshift.com/container-platform/4.8/welcome/index.html)

    - [Open Data Foundation 4.8](https://access.redhat.com/documentation/en-us/red_hat_openshift_container_storage/4.8/html-single/deploying_openshift_container_storage_using_bare_metal_infrastructure/index)

    - [Open Data Hub 1.1](https://opendatahub.io/docs.html)

    - [Anaconda3 Data Science Toolkit](https://www.anaconda.com/products/individual)


## Overview

- High Level Design

![](media/cineca-poc-deployed.drawio.png)

---

- Network Architecture

![](media/cineca-poc-main.drawio.png)

---

- Storage Architecture

![](media/cineca-poc-storage.drawio.png)


## Systems Specification

- Hardware

| Node Role | Spec | Qty | Memory | CPUs/GPUs | Drives |
|:-:	|:-:	|---	|---	|---	|---	|
| kvm host | Precision 7920 CL | 1 | 64GB	| Dual Intel Xeon Platinum 8280L | SSDs 512GB	|
| master | Precision 7920 CL | 3 | 64GB | Dual nVidia Quadro RTX4000 (2x GPU) | SSDs 1TB |
| worker | Precision 7920 CL | 3 | 512GB | Dual nVidia Quadro RTX4000 (2x GPU) | SSDs 1TB |

- DNS Records

__Openshift Zone__

| Hostnames | IPs |
|:-:	|:-:	|
| bastion.ocp4.telnet.nl | 10.10.10.5 |
| proxy-1.ocp4.telnet.nl | 10.10.10.6 |
| proxy-2.ocp4.telnet.nl | 10.10.10.7 |
| master-0.ocp4.telnet.nl | 10.10.10.10 |
| master-1.ocp4.telnet.nl | 10.10.10.11 |
| master-2.ocp4.telnet.nl | 10.10.10.12 |
| worker-0.ocp4.telnet.nl | 10.10.10.20 |
| worker-1.ocp4.telnet.nl | 10.10.10.21 |
| worker-2.ocp4.telnet.nl | 10.10.10.22 |

__Public Zone__

| Hostnames | IPs |
|:-:	|:-:	|
| bastion.corporative.telnet.nl | 120.16.14.5 |
| proxy-1.corporative.telnet.nl | 120.16.14.6 |
| proxy-2.corporative.telnet.nl | 120.16.14.7 |


- Hosts Configuration

__Host: bastion__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| bastion.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.5 | vlan 100 | Openshift network |
| bastion.corporative.telnet.nl | 120.16.14.1 | eno2 | 255.255.128.0 | 120.16.14.5| vlan 110 | Public network/internet |

__Host: proxy-1__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| proxy-1.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.6 | vlan 100 | Openshift network |
| proxy-1.corporative.telnet.nl | 120.16.14.1 | eno2 | 255.255.128.0 | 120.16.14.6| vlan 110 | Public network/internet |

__Host: proxy-2__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| proxy-2.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.7 | vlan 100 | Openshift network |
| proxy-2.corporative.telnet.nl | 120.16.14.1 | eno2 | 255.255.128.0 | 120.16.14.7| vlan 110 | Public network/internet |

__Host: master-0__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| --- | 100.7.25.254 | idRAC | 255.255.0.0 | 100.7.25.10 | vlan 25 | idRAC network |
| master-0.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.10 | vlan 100 | Openshift network |

__Host: master-1__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| --- | 100.7.25.254 | idRAC | 255.255.0.0 | 100.7.25.11 | vlan 25 | idRAC network |
| master-1.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.11 | vlan 100 | Openshift network |

__Host: master-2__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| --- | 100.7.25.254 | idRAC | 255.255.0.0 | 100.7.25.12 | vlan 25 | idRAC network |
| master-2.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.12 | vlan 100 | Openshift network |

__Host: worker-0__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| --- | 100.7.25.254 | idRAC | 255.255.0.0 | 100.7.25.13 | vlan 25 | idRAC network |
| worker-0.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.20 | vlan 100 | Openshift network |

__Host: worker-1__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| --- | 100.7.25.254 | idRAC | 255.255.0.0 | 100.7.25.14 | vlan 25 | idRAC network |
| worker-1.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.20 | vlan 100 | Openshift network |

__Host: worker-2__

| Hostname | Default Gateway | Interface | Netwmask | IPs | VLan | Description |
|:-:	|:-:	|---	|---	|---	|---	|---	|
| --- | 100.7.25.254 | idRAC | 255.255.0.0 | 100.7.25.15 | vlan 25 | idRAC network |
| worker-2.ocp4.telnet.nl  | 10.10.10.1 | eno1 | 255.255.0.0 | 10.10.10.20 | vlan 100 | Openshift network |


## Environment Deployment

- [ ] [Openshift Enterprise 4.8.6 Installation](ocp486-installation.md)
- [ ] [Open Data Foundation 4.8 Installation](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/operators/odf/odf-operator-install.md)
- [ ] [Open Data Hub 1.1 Installation](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/operators/odh/odh-operator-install.md)
- [ ] [Anaconda3 Data Science Toolkit Installation](https://gitlab.com/rcardona/ocp4-apps-examples/-/blob/master/anaconda3/commons-anaconda3.md)

## Extra Use Cases

- [ ] [Configuring HTPasswd Identity Provider](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/identity-management/htpasswd/configuring-identity-provider.md)
- [ ] [Creating Quotas](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/limits-and-quotas/managing-quotas.md)

## Annex

- [ ] [Some Useful Commands Raw](scripts/useful-commands.txt)
- [ ] [Some Useful Commands Titled](scripts/useful-commands-titled.md)


---
